/**
* Built-in Log Configuration
* (sails.config.log)
*
* Configure the log level for your app, as well as the transport
* (Underneath the covers, Sails uses Captain-log for logging, which
* allows for some pretty neat custom transports/adapters for log messages)
*
* For more information on the Sails logger, check out:
* http://links.sailsjs.org/docs/config/log
*/
// Extend Captain-log to use Winston
winston  = require('winston');
module.exports = {
   log: {
      // sails will log everything
      level:'silly',
      // filter the sails log to console and log file using custom Logger
      custom: new (winston.Logger)({
         levels:{
            // redefine log levels
            // Valid `level` configs:
            // i.e. the minimum log level to capture with sails.log.*()
            //
            // 'error'	: Display calls to `.error()`
            // 'warn'	: Display calls from `.error()` to `.warn()`
            // 'info'	: Display calls from `.error()`, `.warn()` to `.info()`
            // 'verbose'	: Display calls from `.error()`, `.warn()`, `.info()` to `.verbose()`
            // 'debug': Display calls from `.error()`, `.warn()`, `.info()`, `.verbose()`, to `.debug()`
            // 'silly': Display calls from `.error()`, `.warn()`, `.info()`, `.verbose()`, `.debug()` to `.silly()`
            'error' : 5, 'warn':4, 'info':3, 'verbose':2, 'debug':1,'silly':0
         },
         transports: [
         new (winston.transports.Console)({
            // Display calls from `.error()`, `.warn()`, to `.info()`
            level:'info'
         }),
         new (winston.transports.File)({
            // Display calls from `.error()`, `.warn()`, `.debug()`, `.info()`, `.verbose()` to `.silly()`
            timestamp:true,
            colorize:true,
            level:'silly',
            json:false,
            filename: 'logs/all.log',
         })
         ]
      })
   }

};
