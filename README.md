Jira Time Trakcing
==================

# Summary

A time tracking web app that allows jira users logging their working time easily. Time log can be tracked automatically or added manually. Besides, user can create new issues corresponding to what they are doing at the moment

# Overview

This project is for a responsive web user interface, interfacing to the jira restful API for time tracking

# High-level requirements:

##### Webapp built on top of node.js
##### Display views:
* Login view via jira oauth
* list all projects
* list all issues of each project
* start time tracking for chosen issue
* create new issue for time tracking
* manually add time log

# Stack

* Environment: Node.JS (on linux)
* Language: javascript/coffeescript
* Framework: sailsjs
* Authentication: jira oauth
* Presentation: Handlebars
* Unit Testing: unitjs
* Logging: Winston 
* Browser framework: backbone.js.