 # OauthController
 #
 # @description :: Server-side logic for managing oauths
 # @help        :: See http://links.sailsjs.org/docs/controllers
fs = require 'fs'
OAuth = require('node-oauth').OAuth
authorizeUrl = ''
issueUrl = 'https://designveloper.atlassian.net/rest/api/2/issue/FU-45'
reqTokenUrl = 'https://designveloper.atlassian.net/plugins/servlet/oauth/request-token'
accessTokenUrl = 'https://designveloper.atlassian.net/plugins/servlet/oauth/access-token'
authorizeUrl = 'https://designveloper.atlassian.net/plugins/servlet/oauth/authorize?oauth_token='
consumerKey = "oauth-sample-consumer"
privateKeyData = fs.readFileSync 'rsa.pem', 'utf8'
consumer = new OAuth reqTokenUrl, accessTokenUrl, consumerKey, "",
  "1.0", "http://localhost:1337/oauth/callback", "RSA-SHA1", null, privateKeyData
module.exports =
  index: (req, res) ->
    sails.log.info "private key data: " + privateKeyData
    consumer.getOAuthRequestToken (err, requestToken, requestSecret, results)->
      if err
        sails.log.error err.data
        res.send 'Error getting OAuth access token'
      else
        req.session.requestToken = requestToken
        req.session.requestSecret = requestSecret
        sails.log.info 'Request token: ' + requestToken
        sails.log.info 'Request secret: ' + requestSecret
        res.redirect authorizeUrl + requestToken
      return
    return

  callback: (req, res) ->
    consumer.getOAuthAccessToken req.session.requestToken,
      req.session.requestSecret,
      req.query.oauth_verifier,
      (err, accessToken, accessSecret, results) ->
        if err
          sails.log.error err.data
          res.send "Error getting access token"
        else
          req.session.accessToken = accessToken
          req.session.accessSecret = accessSecret
          consumer.get issueUrl, accessToken, accessSecret,
            "application/json",
            (err, data, response) ->
              if err
                sails.log.info "error getting issue data"
              sails.log.info "got issue data"
              sails.log.info data
              data = JSON.parse data
              res.send "I am looking at #{data['key']}"
              return
        return
    return
