 # MainController
 #
 # @description :: Server-side logic for managing mains
 # @help        :: See http://links.sailsjs.org/docs/controllers

module.exports =
   index: (req,res) ->
      if req.user
         sails.log.info "Logged in user:"
         sails.log.info req.user
      res.view
         partials:
            header: '../partials/site/header'
            footer: '../partials/site/footer'
         data: 123
